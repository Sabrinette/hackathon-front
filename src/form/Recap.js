
import React, { Component } from 'react';
import { List, ListItem, ListItemText } from '@material-ui/core/';

export class Recap extends Component {
     

  continue = e => {
    e.preventDefault();
    this.props.nextStep();
    // PROCESS FORM //
     // get form data out of state
     const { values } = this.props;

     
     const type = values.type;
     const status = "wainting for validation";
    const creationDate = Date.now();
     const idAuthor = 1;

     const siret= values.siret;
      const phone= values.phone;
        const companyLinkSirene= 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/'+ siret;
      const siretInternship= values.siretInternship;
      const phoneInternship= values.phoneInternship;
    const internshipAddressLinkSirene = 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/'+ siretInternship;

     const title = values.title;
     const titleWithConventionLanguage = values.titleWithConventionLanguage;
     const companyDescription = values.companyDescription 
     const collaboratorsNumber = values.collaboratorsNumber;
     const whyResearchInternship = values.whyResearchInternship; 
     const missionsAndObjectives = values.missionsAndObjectives;  
     const conventionLanguage = values.conventionLanguage;
     const startDate = values.startDate;
     const endDate = values.endDate;
     const workedHoursPerWeek = values.workedHoursPerWeek;
     const workedDaysPerWeek = values.workedDaysPerWeek;
     const campus = values.campus;
     const taf = values.taf ;
     const ieltsLevel = values.ieltsLevel;
     const salary = values.salary; 
     const advantages = values.advantages;

     const civility = values.civility;
     const lastName = values.lastName;
     const firstname = values.firstname;
     const occupation = values.occupation;
     const email = values.email; 
     const phoneSupervisor = values.email;

     const civilitySignatory = values.civilitySignatory; 
     const lastNameSignatory = values.lastNameSignatory;
     const firstnameSignatory = values.firstnameSignatory;
     const occupationSignatory = values.occupationSignatory ;
     const emailSignatory = values.emailSignatory; 
     const phoneSignatory = values.phoneSignatory;

     const phoneContact = values.phoneContact;
     const phoneEmergencyContact = values.phoneEmergencyContact;
     const lastNameEmergencyContact = values.lastNameEmergencyContact;
     const firstNameEmergencyContact = values.firstNameEmergencyContact;
     const relationshipType = values.relationshipType;
  

     const companyAddress = {phone,companyLinkSirene  }
     const internshipAddress = {phoneInternship,internshipAddressLinkSirene,}
     const internshipInformations = {title, titleWithConventionLanguage, companyDescription, collaboratorsNumber,whyResearchInternship,missionsAndObjectives, conventionLanguage, startDate, endDate,workedHoursPerWeek, workedDaysPerWeek, campus,taf, ieltsLevel,salary, advantages,}
     const companySupervisor = {civility,lastName,firstname,occupation,email, phoneSupervisor,}
     const companySignatory = {civilitySignatory,lastNameSignatory,firstnameSignatory,occupationSignatory,emailSignatory, phoneSignatory,}
     const studentContact ={phoneContact, lastNameEmergencyContact, firstNameEmergencyContact,phoneEmergencyContact, relationshipType,}

     const data = {type , status, creationDate,idAuthor, companyAddress, internshipAddress, internshipInformations, companySupervisor, companySignatory, studentContact};

    console.log(data);
    
    const url = 'https://testdemo.osc-fr1.scalingo.io/hackathon';
        fetch(url,{
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
         body: JSON.stringify(data) 
         })
         .then(response => response.json())
         .then(data => {
           console.log('Success:', data);
         })
         .catch((error) => {
           console.error('Error:', error);
         }); 
    };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: { name, siret, phone, conventionAddress, zipCode, city, country, 
                nameInternship, siretInternship,phoneInternship, conventionAddressInternship, zipCodeInternship, cityInternship
                , countryInternship, 
                title, titleWithConventionLanguage, companyDescription, collaboratorsNumber,whyResearchInternship,missionsAndObjectives, conventionLanguage, startDate, endDate,workedHoursPerWeek, workedDaysPerWeek, campus,taf, ieltsLevel,salary, advantages,
                civility,lastName,firstname,occupation,email, phoneSupervisor,
                civilitySignatory,lastNameSignatory,firstnameSignatory,occupationSignatory,emailSignatory, phoneSignatory,
                phoneContact, phoneEmergencyContact, lastNameEmergencyContact, firstNameEmergencyContact, relationshipType,
                cv, fiche,protection, certif,
                civilite3,
                type
            }
    } = this.props;
    return (
            <>
            
            <h2><center>Fiche de stage</center></h2>
            <List id="recap">
            <h5>Adresse de l'Entreprise (envoi de la convention)</h5>
            <ListItem>
                <ListItemText primary="Siret" secondary={siret} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Nom Entreprise" secondary={name} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Adresse de convention" secondary={conventionAddress} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Code postal entreprise" secondary={zipCode} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Ville entreprise" secondary={city} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Pays entreprise" secondary={country} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Téléphone entreprise" secondary={phone} />
            </ListItem>
          
            <hr/>

            <h5>Adresse du lieu du stage (si différent de l'adresse d'envoi de la convention)</h5>
           
            <ListItem>
                <ListItemText primary="Siret 1" secondary={siretInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Nom Entreprise -1-" secondary={nameInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Adresse entreprise -1- " secondary={conventionAddressInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Code postal -1-" secondary={zipCodeInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Ville entreprise -1-" secondary={cityInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Pays entreprise -1-" secondary={countryInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Téléphone entreprise -1-" secondary={phoneInternship} />
            </ListItem>
          
            <hr/>

            <h5> Informations sur le Stage</h5>
            <ListItem>
                <ListItemText primary="Intitulé du stage" secondary={title} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Intitulé du stage dans la langue de la convention " secondary={titleWithConventionLanguage} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Description de l'entreprise" secondary={companyDescription} />
            </ListItem>
            <ListItem>
                <ListItemText primary=" Effectif sur le lieu du stage" secondary={collaboratorsNumber} />
            </ListItem>
            <ListItem>
                <ListItemText primary=" Argumentaire pour le choix de stage de recherche" secondary={whyResearchInternship} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Missions et objectifs" secondary={missionsAndObjectives} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Langue de la convention" secondary={conventionLanguage} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Date début du stage" secondary={startDate} />
            </ListItem>
            <ListItem>
            <ListItemText primary=" Date fin du stage" secondary={endDate} />
            </ListItem>
            <ListItem>
            <ListItemText primary="Nombre d'heures travaillées par semaine" secondary={workedHoursPerWeek} />
            </ListItem>
            <ListItem>
            <ListItemText primary="Jours travaillés et horaires de travail dans la langue de la convention " secondary={workedDaysPerWeek} />
            </ListItem>
            <ListItem>
            <ListItemText primary="Campus d'origine" secondary={campus} />
            </ListItem>
            <ListItem>
            <ListItemText primary="TAF" secondary={taf} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Niveau IELTS" secondary={ieltsLevel} />
            </ListItem>

            <ListItem>
                <ListItemText primary="Indemnité mensuelle BRUTE perçue" secondary={salary} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Avantages dans la langue de la convention" secondary={advantages} />
            </ListItem>
            <hr/>

            <h5>Information tuteur entreprise</h5>
            <ListItem>
                <ListItemText primary="Civilité tuteur entreprise" secondary={civility} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Nom Tuteur entreprise" secondary={lastName} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Prénom tuteur entreprise" secondary={firstname} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Fonction - Poste occupé tuteur entreprise " secondary={occupation} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Email tuteur entreprise (texte)" secondary={email} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Telephone tuteur entreprise" secondary={phoneSupervisor} />
            </ListItem>
            <hr/>
            <h5>Information signataire entreprise</h5>
            <ListItem>
                <ListItemText primary="Civilité signataire entreprise" secondary={civilitySignatory} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Nom signataire entreprise" secondary={lastNameSignatory} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Prénom signataire entreprise" secondary={firstnameSignatory} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Fonction - Poste occupé signataire entreprise " secondary={occupationSignatory} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Email signataire entreprise (texte)" secondary={emailSignatory} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Telephone signataire entreprise" secondary={phoneSignatory} />
            </ListItem>
            <hr/>
            <h5> Contacter l'élève pendant son stage</h5>  
            <ListItem>
                <ListItemText primary=" Téléphone élève pendant le stage" secondary={phoneContact} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Nom personne à contacter en cas d'urgence" secondary={lastNameEmergencyContact} />
            </ListItem>
            <ListItem>
                <ListItemText primary=" Prénom personne à contacter en cas d'urgence" secondary={firstNameEmergencyContact} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Téléphone personne à contacter en cas d'urgence " secondary={phoneEmergencyContact} />
            </ListItem>
           
            <ListItem>
                <ListItemText primary="Lien de parenté personne à contacter en cas d'urgence " secondary={relationshipType} />
            </ListItem>

            <hr/>
            <h5> Comment avez vous trouvé votre stage ?	</h5>
            <ListItem>
                <ListItemText primary="" secondary={civilite3} />
            </ListItem>
            <hr/>    
            <h5>Pièces jointes (Formulaires sur MOODLE) </h5>
            <ListItem>
                <ListItemText primary="CV" secondary={cv} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Fiche de liaison complétée par ent (Anx B1/B2)" secondary={fiche} />
            </ListItem>
            
            <h6>Uniquement pour les départs à l'étranger</h6>
            <ListItem>
                <ListItemText primary="Protection sociale si stage à l'étranger (Anx C1)" secondary={protection} />
            </ListItem>
            <ListItem>
                <ListItemText primary="Certificat Accident travail si>554,40€/m (Anx C2)" secondary={certif} />
            </ListItem>
            <hr/>
            <h5> Type de stage	</h5>
            <ListItem>
                <ListItemText primary="" secondary={type} />
            </ListItem>
            <ListItem>
                <input id="but4" type="submit" value="Valider" onClick={this.continue} />
            </ListItem>
        </List><br />
        </> 
          
    );
  }
}

export default Recap;
