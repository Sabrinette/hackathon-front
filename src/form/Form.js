import React, { Component } from 'react';
import Confirm from './Confirm';
import Page1 from './Page1';
import Recap from './Recap';


export class Form extends Component {
 

  state = {
    step: 1,
    name: '',
    siret: '',
    phone: '',
    conventionAddress: '',
    zipCode: '',
    city: '',
    country:'',
    nameInternship: '',
    siretInternship: '',
    phoneInternship: '',
    conventionAddressInternship: '',
    zipCodeInternship: '',
    cityInternship: '',
    countryInternship:'',
    title:'', 
    titleWithConventionLanguage:'', 
    companyDescription:'', 
    missionsAndObjectives:'',
    collaboratorsNumber:'', 
    whyResearchInternship:'',
    conventionLanguage:'', 
    startDate:'', 
    endDate:'', 
    workedHoursPerWeek:'', 
    workedDaysPerWeek:'', 
    campus:'',
    taf:'', 
    ieltsLevel:'',
    salary:'',
    advantages:'', 
    civility:'', 
    lastName:'', 
    firstname:'', 
    occupation:'', 
    email:'', 
    phoneSupervisor:'', 
    civilitySignatory:'', 
    lastNameSignatory:'', 
    firstnameSignatory:'', 
    occupationSignatory:'', 
    emailSignatory:'', 
    phoneSignatory:'', 
    phoneContact:'',
    lastNameEmergencyContact:'',
    firstNameEmergencyContact:'',
    phoneEmergencyContact:'',
    relationshipType:'',
    cv:'',
    fiche:'',
    protection:'',
    certif:'',
    civilite3:'',
    type:'',

  };
  handleSelect = index => {
    this.setState({ step: index });
  };

  handleButtonClick = () => {
    this.setState({ step: 2 });
  };

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  // Handle fields change
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
//43261042600046
//47976684200138

      if((document.getElementById("siret"))) {
      fetch('https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/'+ document.getElementById("siret").value,
        {
            method: 'GET'
          })
          .then(response => response.json())
          .then(data => {
           this.state.name=data.etablissement.unite_legale.denomination 
           this.state.zipCode=data.etablissement.code_postal
           this.state.conventionAddress=data.etablissement.geo_adresse
           this.state.city=data.etablissement.libelle_commune
           this.state.country="France"

           document.getElementById("name").value=data.etablissement.unite_legale.denomination
           document.getElementById("zipCode").value=data.etablissement.code_postal
           document.getElementById("conventionAddress").value=data.etablissement.geo_adresse
           document.getElementById("city").value=data.etablissement.libelle_commune
           document.getElementById("country").value="France" 
          });
          return;
        }
        if((document.getElementById("siretInternship"))) {
          fetch('https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/'+ document.getElementById("siretInternship").value,
            {
                method: 'GET'
              })
              .then(response => response.json())
              .then(data => {
               this.state.nameInternship=data.etablissement.unite_legale.denomination 
               this.state.zipCodeInternship=data.etablissement.code_postal
               this.state.conventionAddressInternship=data.etablissement.geo_adresse
               this.state.cityInternship=data.etablissement.libelle_commune
               this.state.countryInternship="France"
    
               document.getElementById("nameInternship").value=data.etablissement.unite_legale.denomination
               document.getElementById("zipCodeInternship").value=data.etablissement.code_postal
               document.getElementById("conventionAddressInternship").value=data.etablissement.geo_adresse
               document.getElementById("cityInternship").value=data.etablissement.libelle_commune
               document.getElementById("countryInternship").value="France" 
              });
              return;
            }

      }
   

FileChange= input => e => {
  var files = e.target.files;
  console.log(files);
  var filesArr = Array.prototype.slice.call(files);
  console.log(filesArr);
  this.setState({ [input]: [...this.state.files, ...filesArr] });
}

removeFile(f) {
     this.setState({ files: this.state.files.filter(x => x !== f) }); 
}
  render() {
    const { step } = this.state;
    const { name,groupe, siret,phone, conventionAddress, zipCode, city, country,
            nameInternship, siretInternship,phoneInternship, conventionAddressInternship, zipCodeInternship, cityInternship, countryInternship, 
            title, titleWithConventionLanguage, companyDescription, collaboratorsNumber,whyResearchInternship,missionsAndObjectives, conventionLanguage, startDate, endDate,workedHoursPerWeek, workedDaysPerWeek, campus,taf, ieltsLevel,salary, advantages,
            civility,lastName,firstname,occupation,email, phoneSupervisor,
            civilitySignatory,lastNameSignatory,firstnameSignatory,occupationSignatory,emailSignatory, phoneSignatory,
            phoneContact, phoneEmergencyContact, lastNameEmergencyContact, firstNameEmergencyContact, relationshipType,
            cv, fiche,protection, certif,
            civilite3,
            type} = this.state;
    const values = {  name,groupe, siret,phone, conventionAddress, zipCode, city, country,
                      nameInternship, siretInternship,phoneInternship, conventionAddressInternship, zipCodeInternship, cityInternship, countryInternship,
                      title, titleWithConventionLanguage, companyDescription, collaboratorsNumber,whyResearchInternship,missionsAndObjectives, conventionLanguage, startDate, endDate,workedHoursPerWeek, workedDaysPerWeek, campus,taf, ieltsLevel,salary, advantages, 
                      civility,lastName,firstname,occupation,email, phoneSupervisor,
                      civilitySignatory,lastNameSignatory,firstnameSignatory,occupationSignatory,emailSignatory, phoneSignatory,
                      phoneContact, phoneEmergencyContact, lastNameEmergencyContact, firstNameEmergencyContact, relationshipType,
                      cv, fiche,protection, certif,
                      civilite3,
                      type};              

    switch (step) {
      case 1:
        return (
          <Page1
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
       
        case 2:
            return (
                <Recap
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    values={values}
                />
                ); 
        case 3: return <Confirm/>                                                                
      default:
        console.log('This is a multi-step form built with React.')
    }
  }
}

export default Form;
