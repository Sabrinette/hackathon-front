import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../style/Tabs.css';
class Tab extends Component {
    static propTypes = {
      activeTab: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
    };
  
    onClick = () => {
      const { label, onClick } = this.props;
      onClick(label);
      fetch('https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/43261042600087'.value,{
          method: 'GET'
        })
        .then(response => response.text())
        .then(data => {
          console.log(data)
        });
    }
  
    render() {
      const {
        onClick,
        props: {
          activeTab,
          label,
        },
      } = this;
  
      let className = 'tab-list-item';
  
      if (activeTab === label) {
        className += ' tab-list-active';
      }
  
      return (
        <li
          className={className}
          onClick={onClick}
        >
          {label}
        </li>
      );
    }
  }
  
  export default Tab;