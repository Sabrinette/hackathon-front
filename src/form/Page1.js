import React from "react";
import '../style/Pages.css';
import Tabs from './Tabs';

export class Page1 extends React.Component {

    continue = e => {
            e.preventDefault();
            this.props.nextStep();

    }
   
    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
            <Tabs>
              <div id="test1" label="Adresse de l'entreprise">
              <b name="companyAddress">Adresse de l'entreprise (envoi de la convention)</b>
              <br></br>
              <form>
                <table>
                    <tbody>
                        <tr>
                            <td><label>SIRET uniquement si stage en France   </label></td>
                            <td><input  placeholder="Entrer le siret de l'entreprise en premier temps (14 Chiffres attachés)" class="textbox" id="siret" type="text" name="siret" onChange={handleChange("siret")} defaultValue={values.siret} required="" /></td>
                        </tr>
                        <tr>
                            <td><label>Nom Entreprise  </label></td>
                            <td><input class="textbox" type="text" id= "name" name="name"  onChange={handleChange("name")} defaultValue={values.name} /></td>
                        </tr>
                        <tr>
                            <td><label>Adresse de convention    </label></td>
                            <td><textarea class="textbox" type="text" id="conventionAddress" name="conventionAddress" onChange={handleChange("conventionAddress")} defaultValue={values.conventionAddress} /></td>
                        </tr>
                        <tr>
                            <td><label>Code postal entreprise   </label></td>
                            <td><input class="textbox" type="text" id="zipCode" name="zipCode"  onChange={handleChange("zipCode")} defaultValue={values.zipCode} /></td>
                        </tr>
                        <tr>
                            <td><label>Ville entreprise   </label></td>
                            <td><input class="textbox" type="text" id="city" name="city"  onChange={handleChange("city")} defaultValue={values.city} /></td>
                        </tr>
                        <tr>
                            <td><label>Pays entreprise  </label></td>
                            <td><input class="textbox" type="text" id="country" name="country"  onChange={handleChange("country")} defaultValue={values.country} /></td>
                        </tr>
                        <tr>
                            <td><label>Téléphone entreprise    </label></td>
                            <td><input class="textbox" type="text" id="phone" name="phone"  onChange={handleChange("phone")} defaultValue={values.phone} /></td>
                        </tr>
                        
                    </tbody>
                </table>

            </form>
               </div>
                <div label="Adresse du lieu du stage">
                <b name="internshipAddress">Adresse du lieu du stage</b>
                 <form>
                <table>
                    <tbody>
                    
                        <tr>
                            <td ><label>SIRET uniquement si stage en France -1- </label></td>
                            <td><input placeholder="Entrer le siret de l'entreprise en premier temps (14 Chiffres attachés)" class="textbox" type="text" id="siretInternship" name="siretInternship" onChange={handleChange("siretInternship")} defaultValue={values.siretInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Nom Entreprise -1-  </label></td>
                            <td><input class="textbox" type="text" id="nameInternship" name="nameInternship"  onChange={handleChange("nameInternship")} defaultValue={values.nameInternship}/></td>
                        </tr>
                        <tr>
                            <td ><label>Adresse de convention  -1-   </label></td>
                            <td><input class="textbox" type="text" id="conventionAddressInternship" name="conventionAddressInternship" onChange={handleChange("conventionAddressInternship")} defaultValue={values.conventionAddressInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Code postal entreprise -1-   </label></td>
                            <td><input class="textbox" type="text" id="zipCodeInternship" name="zipCodeInternship"  onChange={handleChange("zipCodeInternship")} defaultValue={values.zipCodeInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Ville entreprise -1-   </label></td>
                            <td><input class="textbox" type="text" id="cityInternship" name="cityInternship"  onChange={handleChange("cityInternship")} defaultValue={values.cityInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Pays entreprise -1- </label></td>
                            <td><input class="textbox" type="text" id="countryInternship" name="countryInternship"  onChange={handleChange("countryInternship")} defaultValue={values.countryInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Téléphone entreprise -1-    </label></td>
                            <td><input class="textbox" type="text" id="phoneInternship" name="phoneInternship"  onChange={handleChange("phoneInternship")} defaultValue={values.phoneInternship} /></td>
                        </tr>
                      
                      
                    </tbody>

                </table>
            </form>   
                </div>
               
                <div label="Informations sur le Stage">
                <br/><br/><br/><br/>
                <b name="internshipAddress">Informations sur le Stage</b>
                <form>
                <table>
                    <tbody>
                        <tr> 
                            <td ><label>Intitulé du stage  </label></td>
                            <td><input class="textbox" type="text" id="title" name="title"  onChange={handleChange("title")} defaultValue={values.title}/></td>
                        </tr>
                        <tr>
                            <td ><label>Intitulé du stage dans la langue de la convention       </label></td>
                            <td><input class="textbox" type="text" id="titleWithConventionLanguage" name="titleWithConventionLanguage" onChange={handleChange("titleWithConventionLanguage")} defaultValue={values.titleWithConventionLanguage} /></td>
                        </tr>
                        <tr>
                            <td ><label> Description de l'entreprise  </label></td>
                            <td><textarea class="textbox" type="text" id="companyDescription" name="companyDescription" onChange={handleChange("companyDescription")} defaultValue={values.companyDescription} /></td>
                        </tr>
                        <tr>
                            <td ><label> Missions et objectifs   </label></td>
                            <td><textarea class="textbox" type="text" id="missionsAndObjectives" name="missionsAndObjectives" onChange={handleChange("missionsAndObjectives")} defaultValue={values.missionsAndObjectives} /></td>
                        </tr>
                        <tr>
                            <td ><label> Effectif sur le lieu du stage    </label></td>
                            <td><input class="textbox" type="text" id="collaboratorsNumber" name="collaboratorsNumber"  onChange={handleChange("collaboratorsNumber")} defaultValue={values.collaboratorsNumber} /></td>
                        </tr>
                        <tr>
                            <td ><label>Argumentaire pour le choix de stage de recherche </label></td>
                            <td><textarea class="textbox" type="text" id="whyResearchInternship" name="whyResearchInternship"  onChange={handleChange("whyResearchInternship")} defaultValue={values.whyResearchInternship} /></td>
                        </tr>
                        <tr>
                            <td ><label>Langue de la convention  </label></td>
                            <td>
                            <select id = "dropdown" name="conventionLanguage"  onChange={handleChange("conventionLanguage")} defaultValue={values.conventionLanguage}>
                                <option value="N/A"></option>
                                <option value="Français">Français</option>
                                <option value="Anglais">Anglais</option>
                            </select>    
                            </td>
                        </tr>
                        <tr>
                            <td ><label>Date début du stage   </label></td>
                            <td><input class="date"  type="date" id="startDate" name="startDate" min="2022-01-01" max="2027-12-31"  onChange={handleChange("startDate")} defaultValue={values.startDate} /></td>
                        </tr>
                        <tr>
                            <td ><label>Date fin du stage   </label></td>
                            <td><input class="date" type="date" id="endDate" name="endDate" min="2022-01-01" max="2027-12-31"  onChange={handleChange("endDate")} defaultValue={values.endDate} /></td>
                        </tr>
                        
                        <tr>
                            <td ><label>Nombre d'heures travaillées par semaine  </label></td>
                            <td><input class="textbox" type="text" id="workedHoursPerWeek" name="workedHoursPerWeek"  onChange={handleChange("workedHoursPerWeek")} defaultValue={values.workedHoursPerWeek} /></td>
                        </tr>
                        <tr>
                            <td ><label>Jours travaillés et horaires de travail dans la langue de la convention</label></td>
                            <td><textarea class="textbox" type="text" id="workedDaysPerWeek" name="workedDaysPerWeek"  onChange={handleChange("workedDaysPerWeek")} defaultValue={values.workedDaysPerWeek} /></td>
                        </tr>
                        <tr>
                            <td ><label>Campus d'origine</label></td>
                            <td><select id = "dropdown" name="campus" id="campus"  onChange={handleChange("campus")} defaultValue={values.campus}>
                                <option value="N/A"></option>
                                <option value="Brest">Brest</option>
                                <option value="Nantes">Nantes</option>
                                <option value="Rennes">Rennes</option>
                                </select>
                                </td>
                        </tr>
                        <tr>
                        <td ><label>TAF</label></td>
                            <td>
                            <select id = "dropdown" name="taf" id="taf" onChange={handleChange("taf")} defaultValue={values.taf}>
                                <option value="N/A"></option>
                                <option value="MSC-MPLP">MSC-MPLP</option>
                                <option value="MSC-PBPE">MSC-PBPE</option>
                                <option value="MSC-NE">MSC-NE</option>
                                <option value="MS-CS">MS-CS</option>
                                <option value="MS-CYMAR">MS-CYMAR</option>
                                <option value="MS-ICD">MS-ICD</option>
                                <option value="TAF-ASCY">TAF-ASCY</option>
                                <option value="TAF-COOC">TAF-COOC</option>
                                <option value="TAF-COPSI">TAF-COPSI</option>
                                <option value="TAF-CYBER">TAF-CYBER</option>
                                <option value="TAF-DASCI">TAF-DASCI</option>
                                <option value="TAF-DCL">TAF-DCL</option>
                                <option value="TAF-DEMIN">TAF-DEMIN</option>
                                <option value="TAF-DEMIN*">TAF-DEMIN*</option>
                                <option value="TAF-DIGIC">TAF-DIGIC</option>
                                <option value="TAF-HEALTH">TAF-HEALTH</option>
                                <option value="TAF-IHM">TAF-IHM</option>
                                <option value="TAF-ILSD*">TAF-ILSD*</option>
                                <option value="TAF-IOT">TAF-IOT</option>
                                <option value="TAF-ISC">TAF-ISC</option>
                                <option value="TAF-LOGIN*">TAF-LOGIN*</option>
                                <option value="TAF-MCE">TAF-MCE</option>
                                <option value="TAF-MPR">TAF-MPR</option>
                                <option value="TAF-NEMO">TAF-NEMO</option>
                                <option value="TAF-OPE">TAF-OPE</option>
                                <option value="TAF-PNUM">TAF-PNUM</option>
                                <option value="TAF-ROBIN">TAF-ROBIN</option>
                                <option value="TAF-SEH">TAF-SEH</option>
                                <option value="TAF-TEE">TAF-TEE</option>
                                <option value="TAF-TEE*">TAF-TEE*</option>
                                <option value="FING-ALL">FING-ALL</option>
                                <option value="FING-GIPAD">FING-GIPAD</option>
                                <option value="FING-GOPL">FING-GOPL</option>
                                <option value="FING-GSI">FING-GSI</option>
                                <option value="FING-IN">FING-IN</option>
                                <option value="FING-MPR">FING-MPR</option>
                                <option value="FING-OMTI">FING-OMTI</option>
                                <option value="FING-QSF">FING-QSF</option>
                                <option value="FING-SEE">FING-SEE</option>
                                <option value="FING-STAR">FING-STAR</option>
                             </select>
                            </td> 

                        </tr>
                        <tr>
                            <td ><label>Niveau IELTS</label></td>
                            <td><input class="textbox" type="text" id="ieltsLevel" name="ieltsLevel"  onChange={handleChange("ieltsLevel")} defaultValue={values.ieltsLevel} /></td>
                        </tr>
                        
                        <tr>
                            <td ><label>Indemnité mensuelle BRUTE perçue  </label></td>
                            <td><input class="textbox" type="text" id="salary" name="salary"  onChange={handleChange("salary")} defaultValue={values.salary} /></td>
                        </tr>
                        <tr>
                            <td ><label>Avantages dans la langue de la convention  </label></td>
                            <td><textarea class="textbox" type="text" id="advantages" name="advantages"  onChange={handleChange("advantages")} defaultValue={values.advantages} /></td>
                        </tr>
                    
                    </tbody>
                    
                </table>
                
            </form>
                </div>
               
                <div label ="Information tuteur entreprise">
                <b name="companySupervisor">Information tuteur entreprise</b>
                <form>
                <table>
                    <tbody>
                        <tr> 
                            <td ><label>  Civilité tuteur entreprise  </label></td>
                            <td>
                            <select id = "dropdown" id="civility" name="civility"  onChange={handleChange("civility")} defaultValue={values.civility}>
                                <option value="N/A"></option>
                                <option value="Monsieur">Monsieur</option>
                                <option value="Madame">Madame</option>
                             </select>
                            </td> 
                        </tr>
                        <tr>
                            <td ><label>Nom Tuteur entreprise </label></td> 
                            <td><input class="textbox" type="text" id="lastName" name="lastName" onChange={handleChange("lastName")} defaultValue={values.lastName} /></td>
                        </tr>
                        <tr>
                            <td ><label>Prénom tuteur entreprise  </label></td>
                            <td><input class="textbox" type="text" id="firstname" name="firstname" onChange={handleChange("firstname")} defaultValue={values.firstname} /></td>
                        </tr>
                        <tr>
                            <td ><label>Fonction - Poste occupé tuteur entreprise    </label></td>
                            <td><input class="textbox" type="text" id="occupation" name="occupation"  onChange={handleChange("occupation")} defaultValue={values.occupation} /></td>
                        </tr>
                        <tr>
                            <td ><label>Email tuteur entreprise (texte)    </label></td>
                            <td><input class="textbox" type="text" id="email" name="email" onChange={handleChange("email")} defaultValue={values.email} /></td>
                        </tr>
                        <tr>
                            <td ><label>Telephone tuteur entreprise   </label></td>
                            <td><input class="textbox" type="text" id="phoneSupervisor" name="phoneSupervisor"  onChange={handleChange("phoneSupervisor")} defaultValue={values.phoneSupervisor} /></td>
                        </tr>
                    </tbody>
                </table>
                
            </form>
                </div>
                <div label ="Information signataire entreprise">
                <b name="companySignatory" >Information signataire entreprise</b>
                <form>
                <table>
                    <tbody>
                        <tr> 
                            <td ><label>  Civilité tuteur entreprise  </label></td>
                            <td>
                            <select id = "dropdown" id="civilitySignatory" name="civilitySignatory"  onChange={handleChange("civilitySignatory")} defaultValue={values.civilitySignatory}>
                                <option value="N/A"></option>
                                <option value="Monsieur">Monsieur</option>
                                <option value="Madame">Madame</option>
                             </select>
                            </td> 
                        </tr>
                        <tr>
                            <td ><label>Nom signataire entreprise </label></td>
                            <td><input class="textbox" type="text" id="lastNameSignatory" name="lastNameSignatory" onChange={handleChange("lastNameSignatory")} defaultValue={values.lastNameSignatory} /></td>
                        </tr>
                        <tr>
                            <td ><label>Prénom signataire entreprise  </label></td>
                            <td><input class="textbox" type="text" id="firstnameSignatory" name="firstnameSignatory" onChange={handleChange("firstnameSignatory")} defaultValue={values.firstnameSignatory} /></td>
                        </tr>
                        <tr>
                            <td ><label>Fonction - Poste occupé signataire entreprise    </label></td>
                            <td><input class="textbox" type="text" id="occupationSignatory" name="occupationSignatory"  onChange={handleChange("occupationSignatory")} defaultValue={values.occupationSignatory} /></td>
                        </tr>
                        <tr>
                            <td ><label>Email signataire entreprise    </label></td>
                            <td><input class="textbox" type="text" id="emailSignatory" name="emailSignatory" onChange={handleChange("emailSignatory")} defaultValue={values.emailSignatory} /></td>
                        </tr>
                        <tr>
                            <td ><label>Telephone signataire entreprise   </label></td>
                            <td><input class="textbox" type="text" id="phoneSignatory" name="phoneSignatory"  onChange={handleChange("phoneSignatory")} defaultValue={values.phoneSignatory} /></td>
                        </tr>
                    </tbody>
                </table>
                </form>
                </div>
                <div label="Contact de l'élève ">
                <br/><br/><br/>
                <b name="studentContact">Contact de l'élève pendant son stage</b>
                <form>
                <table>
                    <tbody>
                        <tr> 
                            <td ><label> Téléphone élève pendant le stage  </label></td>
                            <td><input class="textbox" type="text" id="phoneContact" name="phoneContact" onChange={handleChange("phoneContact")} defaultValue={values.phoneContact} /></td>            
                        </tr>
                        <tr>
                            <td ><label>Téléphone personne à contacter en cas d'urgence  </label></td>
                            <td><input class="textbox" type="text" id="phoneEmergencyContact" name="phoneEmergencyContact" onChange={handleChange("phoneEmergencyContact")} defaultValue={values.phoneEmergencyContact} /></td>
                        </tr>
                        <tr>
                            <td ><label>Nom personne à contacter en cas d'urgence  </label></td>
                            <td><input class="textbox" type="text" id="lastNameEmergencyContact" name="lastNameEmergencyContact" onChange={handleChange("lastNameEmergencyContact")} defaultValue={values.lastNameEmergencyContact} /></td>
                        </tr>
                        <tr>
                            <td ><label>Prénom personne à contacter en cas d'urgence    </label></td>
                            <td><input class="textbox" type="text" id="firstNameEmergencyContact" name="firstNameEmergencyContact"  onChange={handleChange("firstNameEmergencyContact")} defaultValue={values.firstNameEmergencyContact} /></td>
                        </tr>
                        <tr>
                            <td ><label>Lien de parenté personne à contacter en cas d'urgence    </label></td>
                            <td><input class="textbox" type="text" id="relationshipType" name="relationshipType"  onChange={handleChange("relationshipType")} defaultValue={values.lienContact} /></td>
                        </tr>
                        
                    </tbody>
                </table>
                
            </form>
                </div>
               
                <div label="Comment avez vous trouvé votre stage ?">
                <b>Comment avez vous trouvé votre stage ?</b>
                <form>
                <table>
                    {/* <thead>  Comment avez vous trouvé votre stage ?</thead>   */}
                    <tbody>
                        <tr> 
                            <td ><label> Selectionnez   </label></td>
                            <td>
                            <select id = "dropdown" id="civilite3" name="civilite3"  onChange={handleChange("civilite3")} defaultValue={values.civilite3}>
                                <option value="N/A"></option>
                                <option value="Liste de stage sur campus">Liste de stage sur campus</option>
                                <option value="Plateforme JobTeaser">Plateforme JobTeaser</option>
                                <option value="Réponse à une offre">Réponse à une offre</option>
                                <option value="Candidature spontanée">Candidature spontanée</option>
                                <option value="Via un réseau(SIA, Personnel, Entreprise, Autres )">Via un réseau(SIA, Personnel, Entreprise, Autres )</option>
                                <option value="Via un enseignant">Via un enseignant</option>
                             </select>
                            </td> 
                        </tr>
                        
                    </tbody>
                </table>
                {/* <tfoot>
                        <input id="but2" type="submit" value="Retour"  onClick={this.back}/> 
                        <input id="but1" type="submit" value="Suivant"  onClick={this.continue}/> 
                </tfoot> */}
                
            </form>
                </div>
                <div label ="Pièces jointes">
                <b>Pièces jointes (Formulaires sur MOODLE) </b>
                <form>
                <table>
                    {/* <thead> Pièces jointes (Formulaires sur MOODLE) : Pas de validation sans l'ensemble des pièces</thead>   */}
                    <tbody>
                        <tr> 
                            <td ><label>CV  </label></td>
                            <td><input type="file" class="textbox" id="cv" name="cv" onChange={handleChange("cv")} /*onChange={FileChange} defaultValue={values.cv}*//></td>
                        </tr>
                        <tr> 
                            <td ><label>Fiche de liaison complétée par ent (Anx B1/B2)   </label></td>
                            <td><input type="file" class="textbox" id="fiche" name="fiche" onChange={handleChange("fiche")} /*defaultValue={values.fiche}*//></td>
                        </tr>
                        <h6>Uniquement pour les départs à l'étranger :</h6>
                  
                  <tr> 
                      <td ><label>Protection sociale si stage à l'étranger (Anx C1)  </label></td>
                      <td><input type="file" class="textbox" id="protection" name="protection" onChange={handleChange("protection")} defaultValue={values.protection}/></td>
                  </tr>
                  <tr> 
                      <td ><label>Certificat Accident travail (Anx C2)  </label></td>
                      <td><input type="file" class="textbox" id="certif" name="certif" onChange={handleChange("certif")} defaultValue={values.certif}/></td>
                  </tr>
                        
                    </tbody>
                </table>
                {/* <tfoot>
                        <input id="but2" type="submit" value="Retour"  onClick={this.back}/> 
                        <input id="but1" type="submit" value="Suivant"  onClick={this.continue}/> 
                </tfoot> */}
            </form>
                </div>
                <div label="type de stage">
                <b>Sélectionnez le type de stage </b>
                <form>
                <table>
                    {/* <thead>Sélectionnez le type de stage </thead>   */}
                    <tbody>
                        <tr> 
                            <td ><label> Selectionnez   </label></td>
                            <td>
                                <select id = "dropdown" id="type" name="type"  onChange={handleChange("type")} defaultValue={values.type}>
                                    <option value="N/A"></option>
                                    <option value="Stage Ingénierie">Stage Ingénierie</option>
                                    <option value="Stage de fin d'étude">Stage de fin d'étude</option>
                                </select> 
                            </td>        
                            {/* <td ><label>cochez cette case lorsque votre fiche est complète pour que le RUV puisse valider et soumettre votre fiche à l'AE</label></td>
                            <td> <input type="radio" id="huey" name="rue"  onChange={handleChange("rue")} defaultValue={values.rue}/></td> */}
                        </tr>
                        
                    </tbody>
                </table>
                <br/>
                <div>
                        {/* <input id="but2" type="submit" value="Retour"  onClick={this.back}/>  */}
                        <input id="but3" type="submit" value="Soumettre"  onClick={this.continue}/> 
                </div>
                
            </form>
                    
                </div>
              </Tabs>
            
             </div>

        );
    }        

}
export default Page1;

  